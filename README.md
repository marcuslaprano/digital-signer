# README #

> This is a simple example of how to create a digital document signer application.
>
> It's necessary to change this application to run with your code.

# INSTRUCTIONS

> Download the application.

> Import into your eclipse IDE.

> Run maven to update your local repository.

> Develop your solution using this code as guide.


package br.exception;

public class DocumentSignException extends Exception{
	private static final long serialVersionUID = 4295180558780124947L;

	public DocumentSignException(String message) {
		super(message);
	}


}
